package com.services;

import com.entities.Generico;
import com.enumerados.enumeradoGenerico;

public interface Operaciones {
	
	
	Generico Crear(enumeradoGenerico);
	Generico ObtenerTodos(enumeradoGenerico);
	Generico ObtenerPorId(enumeradoGenerico);

}
