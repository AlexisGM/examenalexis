package com.entities;

public class Jugador extends Persona {
	private Equipo equipo;

	public Equipo getEquipo() {
		return equipo;
	}

	public void setEquipo(Equipo equipo) {
		this.equipo = equipo;
	}
	
	
}
