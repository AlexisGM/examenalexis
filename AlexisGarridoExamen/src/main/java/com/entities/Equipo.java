package com.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity 
public class Equipo implements Serializable{
	private static final long serialVersionUID = 3448482279811920813L;

	@Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;

    private String nombreEquipo;
    private int anyoFundacion;
    private String email;
    private List<Jugador> jugadoresEquipo;
    private Entrenador entrenadorEquipo;
    
}
